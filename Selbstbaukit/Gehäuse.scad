BOX = false;
EINSCHUB_SDS_2D = false;
EINSCHUB_SDS_3D = false;
EINSCHUB_PLATINE_2D = false;
EINSCHUB_PLATINE_3D = false;
PLATINE = false;
DECKEL_3D = false;
DECKEL_2D = false;
TUER_2D = false;
TUER_3D = false;
BATTERIE_BOX = false;
SDS011 = false;
KINDERSICHERUNG = false;


rahmen = 0.25;
mutterrahmen = 0.15;
boden = 1.25;
breite = 10 + 2 * rahmen;
laenge = 10 + 2 * rahmen;


breiteEinschub = 1.6 * rahmen; // 0,4 // vorher: 1.3 * rahmen;//0,325
hoeheEinschubGesamt = 2 * rahmen + breiteEinschub; // 0,825



// Schlauchlochradius für SDS011 Zuluft
schlauchlochradius = 0.45 + 0.1; //+0.2

// Lochradius für Löcher in Einschübe
lochradiusSchraubeEinschub = 0.16;

// LOchradius für Reset und Schalter
schalterLochradius = 0.3 + 0.01;//vorher +0,05 //0.6;
resetLochradius = 0.35 + 0.01;//vorher +0,05 //0.7;



// benötigte Höhe für Elektonik mit Platine
platineMitBauteilenHoehe = 2.1; // also Platine + Bauteile + Buchsenhalter

// Freiraum über Platinenbereich
abstandZuPlatinenbereich = 0.5;

// Abstand vo einer Luftlochreihe oben und unten
abstandLuftlochreihen = 0.75;

hoeheSDS011 = 2.6;



hRaum1 = 4; //besitzt noch einen Rahmen
hRaum2 = 2.6 + resetLochradius*2 + 0.4; //besitzt noch zwei Rahmen
hRaum3 = 2.8;//platineMitBauteilenHoehe + abstandLuftlochreihen*2 + 0.2*2; //4;//platineMitBauteilenHoehe + abstandZuPlatinenbereich +resetLochradius*2 + abstandLuftlochreihen + 0.2*2;
//letzteres ist der Durchmesser der Luftlöcher
//4; //5;//3.5; //besitzt noch einen Rahmen

zEinschub1 = hRaum1 + boden;
zEinschub2 = hRaum2 + zEinschub1 + hoeheEinschubGesamt;

zBodenRaum2 = zEinschub1 + hoeheEinschubGesamt - rahmen;
zBodenRaum3 = zEinschub2 + hoeheEinschubGesamt - rahmen;

inhaltHoehe = hRaum1 + hRaum2 + hRaum3 + 2 * hoeheEinschubGesamt;
hoehe = inhaltHoehe + boden;

rundung = 0.1;

einschubLaenge = laenge - 2.3 * rahmen; // 0,3 Luftraum ist 9,925 cm
einschubBreite = breite - hoeheEinschubGesamt; //vorher:  - rahmen //9,425 cm

einschubNutzlaenge = einschubLaenge - 2 * rahmen; //ist 9,425cm
einschubNutzBreite = einschubBreite - rahmen; // ist 9.175 cm

//Platine Länge: ca. 9 cm
//Platine Breite: ca. 8,5 cm

// Höhe für Luftlochreihen
hoeheLuftlochreihen = hoehe - abstandLuftlochreihen; //0.75; //1.25;


/*
// Höhe für Reset und Schalter
//abstandZuPlatinenbereich = 0.5;
hoeheAnAusSchalter = zBodenRaum3 + platineMitBauteilenHoehe + abstandZuPlatinenbereich + (resetLochradius-schalterLochradius)/2;
hoeheReset = zBodenRaum3 + platineMitBauteilenHoehe + abstandZuPlatinenbereich;
*/


// Höhe für Reset und Schalter
hoeheAnAusSchalter = zBodenRaum2 + hoeheSDS011 + (resetLochradius-schalterLochradius)/2;
hoeheReset = zBodenRaum2 + hoeheSDS011;


function mutterboxLaenge(durchmesser) = durchmesser + 0.1 + 2 * rahmen;

// Mutter Deckel und Einschübe
mutterDurchmesser = 0.5;
mutterHoehe = 0.3;
mh = hoehe - mutterHoehe * 1.5 - rahmen;
ml = mutterboxLaenge(mutterDurchmesser); // 1,1 cm

// Mutter Stativ
// Mutter Stativ Durchmesser
mbd = 1.9;
// Mutter Stativ Höhe
mbmh = 0.65;
mbl = mutterboxLaenge(mbd);
mbh = mbmh * 1.5 + rahmen; // 10,25


// Batterieboxraum
batterieBoxLaenge = 7;
batterieBoxBreite = 7;
batterieBoxBeginnX = (laenge - batterieBoxLaenge) / 2;
batterieBoxBeginnY = (breite - batterieBoxBreite) / 2 + 0.5;
batterieBoxWandLaenge = 4;
batterieBoxWandBreite = rahmen;
batterieBoxWandHoehe = 3;




// Platine
platinenDicke = 0.16;
platinenLaenge = 9;
platinenBreite = 8.5;
platinenBeginnX = (einschubLaenge - platinenLaenge) / 2;
platinenBeginnY = (einschubBreite - platinenBreite) / 2;



// Deckel
deckelRandVersatz = 0;//rahmen * 0.15;
deckelLaenge = laenge - 2 * deckelRandVersatz; //10,5 +0
deckelBreite = breite - 2 * deckelRandVersatz; //10,5 +0
deckelDicke = rahmen;
lochradiusSchraubeDeckel = lochradiusSchraubeEinschub;
deckelLuftBeginnX = ml + rahmen; // 1,35
deckelLuftBeginnY = ml + rahmen; // 1,35
deckelLuftLaenge = deckelLaenge - 2 * deckelLuftBeginnX; // 7,8
deckelLuftBreite = deckelBreite - 2 * deckelLuftBeginnY - hoeheEinschubGesamt + 4 * rahmen; // 7,225
luftlochRadiusDeckel = 0.2;
luftlochAnzahlDeckel = 10;
luftlochReiheAbstandDeckel = deckelLuftBreite / (luftlochAnzahlDeckel + 1); // ca. 0,66
luftlochReihenAnzahl = deckelLuftBreite / (luftlochReiheAbstandDeckel + luftlochRadiusDeckel * 2);

// TUER
tuerBreite = breite - 2.3 * rahmen; // 9,925
tuerLaenge = breiteEinschub - 0.3 *rahmen; 
tuerHoehe = 3.1 * rahmen + (hoehe - boden - 0.1 * rahmen); //0.3 * rahmen); 


// Higher definition curves
$fs = 0.01;//01;


// für Gewinde der Kindersicherung
/*
gewindeHoehe = 1.5;
gewindeDurchmesser = 2.2;
gewindeAushoelungDurchmesser = 0.8; //schalterLochradius*2;
gewindeVorsatz = 0.5;
*/

gewindeAbstand = 0.4;//0.35
gewindeHoehe = 2;
gewindeDurchmesser = 2.8;
$fn=50;


module roundedcube(size = [1, 1, 1], center = false, radius = 0.5, apply_to = "all") {
	// If single value, convert to [x, y, z] vector
	size = (size[0] == undef) ? [size, size, size] : size;

	translate_min = radius;
	translate_xmax = size[0] - radius;
	translate_ymax = size[1] - radius;
	translate_zmax = size[2] - radius;

	diameter = radius * 2;

	module build_point(type = "sphere", rotate = [0, 0, 0]) {
		if (type == "sphere") {
			sphere(r = radius);
		} else if (type == "cylinder") {
			rotate(a = rotate)
			cylinder(h = diameter, r = radius, center = true);
		}
	}

	obj_translate = (center == false) ?
		[0, 0, 0] : [
			-(size[0] / 2),
			-(size[1] / 2),
			-(size[2] / 2)
		];

	translate(v = obj_translate) {
		hull() {
			for (translate_x = [translate_min, translate_xmax]) {
				x_at = (translate_x == translate_min) ? "min" : "max";
				for (translate_y = [translate_min, translate_ymax]) {
					y_at = (translate_y == translate_min) ? "min" : "max";
					for (translate_z = [translate_min, translate_zmax]) {
						z_at = (translate_z == translate_min) ? "min" : "max";

						translate(v = [translate_x, translate_y, translate_z])
						if (
							(apply_to == "all") ||
							(apply_to == "xmin" && x_at == "min") || (apply_to == "xmax" && x_at == "max") ||
							(apply_to == "ymin" && y_at == "min") || (apply_to == "ymax" && y_at == "max") ||
							(apply_to == "zmin" && z_at == "min") || (apply_to == "zmax" && z_at == "max")
						) {
							build_point("sphere");
						} else {
							rotate = 
								(apply_to == "xmin" || apply_to == "xmax" || apply_to == "x") ? [0, 90, 0] : (
								(apply_to == "ymin" || apply_to == "ymax" || apply_to == "y") ? [90, 90, 0] :
								[0, 0, 0]
							);
							build_point("cylinder", rotate);
						}
					}
				}
			}
		}
	}
}

module s_thread(diameter, slope, height) {
    if($fn) {mypolygon(diameter, slope, $fn, height);}
    else {mypolygon(diameter, slope, 16, height); }
}

module mypolygon(diameter, slope, polygons, height) {
    n = polygons;
    d = diameter;
    k = slope;
/* Shape:
    g___d
    |    \
    |      \ c
    |       |
    |      / b
    |   a /
    |   |
    ----
    f   e
    */
    m = round(height/slope);
  
echo (height/slope);
    
function cosinus(n,i) = (cos((360/n)*i));
function sinus(n,i) = (sin((360/n)*i));
//function height(k,n,i,offset) = (k/n*i+offset*k)>height ? height : (k/n*i+offset*k)<0 ? 0 : (k/n*i+offset*k);
function height(k,n,i,offset) = (k/n*i+offset*k);
off2=1;    
points = [
/* A */  for(i=[0:n*m]) [(d-k)/2*cosinus(n,i),(d-k)/2*sinus(n,i),height(k,n,i,-0.9+off2)],
/* B */  for(i=[0:n*m]) [d/2*cosinus(n,i),d/2*sinus(n,i),height(k,n,i,-0.5+off2)],
/* C */  for(i=[0:n*m]) [d/2*cosinus(n,i),d/2*sinus(n,i),height(k,n,i,-0.4+off2)],
/* D */  for(i=[0:n*m]) [(d-k)/2*cosinus(n,i),(d-k)/2*sinus(n,i),height(k,n,i,0+off2)],
/* E */  for(i=[0:n*m]) [(d-k)/2*cosinus(n,i),(d-k)/2*sinus(n,i),height(k,n,i,-1+off2)],
/* F */  [0,0,height(k,n,0,-1+off2)],
/* G */  [0,0,height(k,n,n*m,0+off2)],
];

faces = [
/* === lower-faces === */
  for (i=[0:n*m-1]) [i, i+n*m+2, i+1],
  for (i=[0:n*m-1]) [i, i+n*m+1, i+n*m+2],
      
/* === vertical-outside-faces === */
  for (i=[0:n*m-1]) [i+n*m+1, i+n*m*2+2, i+n*m+2],
  for (i=[0:n*m-1]) [i+n*m+2, i+n*m*2+2, i+n*m*2+3],
      
/* === upper-faces === */
  for (i=[0:n*m-1]) [i+n*m*3+3, i+n*m*2+3, i+n*m*2+2],
  for (i=[0:n*m-1]) [i+n*m*3+3, i+n*m*3+4, i+n*m*2+3],
    
/* === vertical-inner-faces === */
  for (i=[0:n*m-1]) [i+n*m*4+4, i, i+1],
  for (i=[0:n*m-1]) [i+n*m*4+4, i+1, i+n*m*4+5],
    
/* === bottom-faces === */
  for(i=[1:n]) [i+n*m*4+3, i+n*m*4+4, 1+n*m*5+4],
  //for(i=[1:n]) [i+n*m*4+4,i+n*m*5+5, i+n*m*5+4],
      
/* === top-faces === */
  for(i=[0:n-1]) [i+n*m*4-n+3, 2+n*m*5+4, i+n*m*4-n+4],

/* === lower-sidewall (endstop) === */
  [n*m*3+3, n*m*2+2, n*m+1, 0, n*m*4+4, n*m*5+5],
  
/* === upper-sidewall (endstop) === */
  [n*m, n*m*2+1, n*m*3+2, n*m*4+3, n*m*5+6, n*m*4-n+3]
]; 

polyhedron(points, faces, convexity=2);
  
}
module rotate_about_pt(z, y, pt, x = 0) {
    translate(pt)
        rotate([x, y, z])
            translate(-pt)
                children();   
}

module right_triangle(side1,side2,corner_radius,triangle_height){
  translate([corner_radius,corner_radius,0]){  
    hull(){  
    cylinder(r=corner_radius,h=triangle_height);
      translate([side1 - corner_radius * 2,0,0])cylinder(r=corner_radius,h=triangle_height);
          translate([0,side2 - corner_radius * 2,0])cylinder(r=corner_radius,h=triangle_height);  
    }
  }
    
}

module einschubHalterung(eLaenge, eBreite, oben = false) {
    // hinten
    translate([0, 0, -rahmen])
        rotate_about_pt(0, 90, [rahmen / 2, 0, rahmen / 2])
            // um *2 vergrößert
            right_triangle(rahmen*2, rahmen*2 , 0.00001, eLaenge);
    cube([eLaenge, rahmen*2, rahmen], false);
    if (oben) {
        translate([0, 0, rahmen + rahmen * 1.3]) {
            cube([eLaenge, rahmen, rahmen], false);
        }
    }
    
    // rechts
    translate([0, eBreite - rahmen, -rahmen])
        rotate_about_pt(90, 180, [rahmen / 2, rahmen / 2, rahmen / 2], 180)
        rotate_about_pt(0, 90, [rahmen / 2, 0, rahmen / 2])
            right_triangle(rahmen, rahmen , 0.00001, eBreite);
    cube([rahmen, eBreite, rahmen], false);
    if (oben) {
        translate([0, 0, rahmen + rahmen * 1.3]) {
            cube([rahmen, eBreite, rahmen], false);
        }
    }
    
    // links
    translate([eLaenge - rahmen, 0, 0]) {
        translate([0, 0, -rahmen])
        rotate_about_pt(-90, 180, [rahmen / 2, rahmen / 2, rahmen / 2], 180)
        rotate_about_pt(0, 90, [rahmen / 2, 0, rahmen / 2])
            right_triangle(rahmen, rahmen , 0.00001, eBreite);
        cube([rahmen, eBreite, rahmen], false);
        if (oben) {
            translate([0, 0, rahmen + rahmen * 1.3]) {
                cube([rahmen, eBreite, rahmen], false);
            }
        }
    }
}

module einschubBodenplatten(zPosition) {
    translate([rahmen, breite - hoeheEinschubGesamt, zPosition + rahmen]) {
        cube([rahmen, hoeheEinschubGesamt, breiteEinschub], false);
    }
    translate([laenge - 2 * rahmen, breite - hoeheEinschubGesamt, zPosition + rahmen]) {
        cube([rahmen, hoeheEinschubGesamt, breiteEinschub], false);
    }
}

/**
 * @param durchmesser
 * @param stuetzen [hinten = 0, rechts = 1, vorne = 2, links = 3]
 */
module MutterHalterung(durchmesser, mHoehe, stuetzen = [], inWand = true) {
    innenFl = durchmesser + 0.1;
    innenH = mHoehe * 1.5;
    laengeGes = innenFl + 2 * rahmen;
    difference() {
        cube([laengeGes, laengeGes, innenH + rahmen], false);
        translate([rahmen, rahmen, rahmen]) {
            cube([innenFl, innenFl, innenH], false);
            //translate([innenFl / 2, innenFl / 2, 0])
                //cylinder(innenH, innenFl / 2, innenFl / 2, false, $fn = 6);
        }
    }
    for (pos = stuetzen) {
        a = pos % 2;
        b = a * -1;
        c = (1 + pos) % 2;
        d = c * -1;
        
        x = ((pos == 3) ? -1 : (pos == 1) ? 1 : 0);
        translate([x * laengeGes / 2, a * laengeGes / 2, - laengeGes])
            rotate_about_pt(pos * 90, 90, [laengeGes / 2, 0, laengeGes / 2])
            translate([0, inWand ? rahmen : 0, 0])
                right_triangle(laengeGes, laengeGes - (inWand ? rahmen : 0), 0.00001, ml);
    }

}

module EinschnittInEinschubHalterung(durchmesser, mHoehe, stuetzen = [], inWand = true) {
    innenFl = durchmesser + 0.1;
    innenH = mHoehe * 1.5;
    translate([rahmen, rahmen, rahmen]) {
            cube([innenFl, innenFl, innenH], false);
    }
}

//Text mit Bogen erzeugen
module revolve_text(radius, chars) {
    PI = 3.14159;
    circumference = 2 * PI * radius;
    chars_len = len(chars);
    font_size = circumference / chars_len;
    step_angle = 150 / chars_len;
    for(i = [0 : chars_len - 1]) {
        rotate(i * step_angle) 
            translate([0, -radius + -font_size / 2, 0])
                //rotate_about_pt(90, 90, [0, -radius + -font_size / 2, 0])
                linear_extrude(0.2)
                text(
                    chars[i], 
                    font = "Courier New; Style = Bold", 
                    size = font_size, 
                    valign = "center", halign = "center"
                );
    }
}

module batterieBoxWaende() {
    // hinten
    translate([laenge/2 - (batterieBoxLaenge/2) + ((batterieBoxLaenge-batterieBoxWandLaenge)/2), batterieBoxBeginnY - rahmen, boden])
        cube([batterieBoxWandLaenge, batterieBoxWandBreite, batterieBoxWandHoehe], false);
    
    
    
    // rechts
    translate([batterieBoxBeginnX - rahmen, breite/2 - (batterieBoxBreite/2) + ((batterieBoxBreite-batterieBoxWandLaenge)/2), boden])
        cube([batterieBoxWandBreite, batterieBoxWandLaenge, batterieBoxWandHoehe], false);
    
    
    
    // links
    translate([batterieBoxBeginnX + batterieBoxLaenge, breite/2 - (batterieBoxBreite/2) + ((batterieBoxBreite-batterieBoxWandLaenge)/2), boden])
        cube([batterieBoxWandBreite, batterieBoxWandLaenge, batterieBoxWandHoehe], false);
}

module Box() {
    union() {
        // Aussenhuelle
        difference() {
            roundedcube([laenge, breite, hoehe], false, rundung);
            // Innenraum
            translate([rahmen, rahmen, boden]) {
                cube([laenge - 2 * rahmen, breite, hoehe], false);
            }
            
            // Vertiefung fuer die Tuer
            translate([rahmen, breite - rahmen - breiteEinschub, boden - 3*rahmen ]) {
                cube([breite - 2 * rahmen, breiteEinschub, 3.1 * rahmen]);
            }
           
            
            // Loch für Stativmutterhalterung
            translate([(laenge - mbl) / 2, (breite - mbl) / 2, 0])
                cube([mbl, mbl, mbh], false);
            translate([(laenge - mbl) / 2, (breite - mbl) / 2, 0])
                cube([mbl, mbl, rahmen], false);
            
            
            // Luftlöcher bei Raum 3
            // Luftloecher hinten
            translate([0.6, 0, hoeheLuftlochreihen])
                Luftlochreihe(laenge - 2 * 0.6, 0.2, 10);
            
            // Luftlöcher rechts
            translate([0, 0.6, hoeheLuftlochreihen]) 
                rotate_about_pt(90, 0, [0, rahmen, 0])
                    Luftlochreihe(laenge - hoeheEinschubGesamt - 2 * 0.6, 0.2, 9);
            
            // Luftlöcher links
            translate([laenge-rahmen,  0.6, hoeheLuftlochreihen]) 
                rotate_about_pt(90, 0, [0, rahmen, 0])
                    Luftlochreihe(laenge - hoeheEinschubGesamt - 2 * 0.6, 0.2, 9);
            
            
            // Loch für An-/Aus-Schalter (hinten links, also weiter weg vom KUS)      
            translate([laenge - (laenge/2)/2 - schalterLochradius, 0, hoeheAnAusSchalter])
                rotate_about_pt(90, 90, [schalterLochradius, 0, rahmen/2]) 
                cylinder(rahmen*2, schalterLochradius, schalterLochradius, false);
                
            // Loch für Kindersicherunghalterung rechts
            translate([laenge - (laenge/2)/2 - schalterLochradius/2 - gewindeDurchmesser/2 - (gewindeDurchmesser/8) -0.045, 0, hoeheAnAusSchalter + (schalterLochradius-0.2)])
                rotate_about_pt(90, 90, [0.2, 0, rahmen/2]) 
                    cylinder(rahmen*2, 0.2, 0.2, false);
                    
            // Loch für Kindersicherunghalterung links
            translate([laenge - (laenge/2)/2 - schalterLochradius/2 + gewindeDurchmesser/2 + (gewindeDurchmesser/8) -0.045, 0, hoeheAnAusSchalter + (schalterLochradius-0.2)])
                rotate_about_pt(90, 90, [0.2, 0, rahmen/2]) 
                    cylinder(rahmen*2, 0.2, 0.2, false);
                                      
                         
            // Loch für Reset (hinten rechts)     
            translate([(laenge/2)/2 - resetLochradius, 0, hoeheReset])
                rotate_about_pt(90, 90, [resetLochradius, 0, rahmen/2]) 
                cylinder(rahmen*2, resetLochradius, resetLochradius, false);
            
            
            // Luftlöcher bei Raum 2: SDS011 Abluft
            translate([0, 0, zBodenRaum2 + 1 ]) 
                rotate_about_pt(90, 0, [0, rahmen, 0])
                    Luftlochreihe(5.5, 0.2, 6);
            translate([0, 0, zBodenRaum2 + 1 + 0.4 + 0.3 ]) 
                rotate_about_pt(90, 0, [0, rahmen, 0])
                    Luftlochreihe(5.5, 0.2, 6);
            
            // Luftlöcher bei Raum 2: SDS011 Zuluft / Schlauchloch
            translate([laenge - rahmen, 3.8 - 0.32 + einschubLaenge / 2 - 7 / 2, zBodenRaum2 - 0.8 * rahmen + 0.5]) 
                rotate_about_pt(90, 0, [0, rahmen, 0])
                    rotate_about_pt(90, 90, [0.45, 0, rahmen / 2])
                        cylinder(rahmen * 1.5, schlauchlochradius, schlauchlochradius, false);
                        
            
            // Lüffi hinten reinschneiden
            radius = 1.5;
            chars = "  Lüffi";
            translate([laenge/2, 0.03, 2.5*hoehe/5])
                rotate_about_pt(-90, 90, [0, -radius -font_size / 2, 0])
                    revolve_text(radius, chars);
        }
        
       

        // Einschube
        difference() {
            // Einschuebe links und rechts (vertikal fuer tuer)
            union() {
                // links vorne
                
                translate([laenge - 2 * rahmen, breite - rahmen, boden - rahmen]) {
                    translate([rahmen, 0, 0]) rotate([0, 0, 90]) union() {
                        roundedcube([rahmen, rahmen, inhaltHoehe + rahmen], false, rundung, "y");
                        translate([0, 0, 0]) cube([0.5 * rahmen, rahmen, inhaltHoehe + rahmen], false);
                        cube([rahmen, rahmen, 1], false);
                    }
                }
                // links hinten
                translate([laenge - 2 * rahmen, breite - (2 * rahmen + breiteEinschub), boden - rahmen]) {
                    cube([rahmen, rahmen, inhaltHoehe + rahmen], false);
                }
                // rechts vorne
                translate([rahmen, breite - rahmen, boden - rahmen]) {
                    translate([rahmen, 0, 0]) rotate([0, 0, 90]) union() {
                        roundedcube([rahmen, rahmen, inhaltHoehe + rahmen], false, rundung, "y");
                        translate([0, 0, 0]) cube([0.5 * rahmen, rahmen, inhaltHoehe + rahmen], false);
                        cube([rahmen, rahmen, 1], false);
                    }
                }
                // rechts hinten
                translate([rahmen, breite - (2 * rahmen + breiteEinschub), boden - rahmen]) {
                    cube([rahmen, rahmen, inhaltHoehe + rahmen], false);
                }
            }
            
            // Einschuebe bodenplatten
            einschubBodenplatten(zEinschub1);
            einschubBodenplatten(zEinschub2);
        }
        

        // Einschubhalterung für SDS011
        translate([rahmen, rahmen, zEinschub1]) {
            einschubHalterung(laenge - 2 * rahmen, breite - breiteEinschub - 3 *rahmen, false);
        }
        
        // Einschubhalterung für Platine
        difference() {
            translate([rahmen, rahmen, zEinschub2]) {
                einschubHalterung(laenge - 2 * rahmen, breite - breiteEinschub - 3 *rahmen);
            }
            //da Mutterhalterung innerhalb der Einschubhalterung aufgrund von Platzeinsparungen sein soll, wird hier die Lücke in der Einschubhalterung erzeugt
            // rechts
            translate([0, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 * 2 - 0.15 * rahmen , zEinschub2 - mutterHoehe * 1.5]) EinschnittInEinschubHalterung(mutterDurchmesser, mutterHoehe+0.01, [3], true);
            // links
            translate([laenge - ml, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 - 0.15 * rahmen , zEinschub2 - mutterHoehe * 1.5]) EinschnittInEinschubHalterung(mutterDurchmesser, mutterHoehe+0.01, [3], true);
        }
        

        
        // Mutterhalterung Deckel
        translate([0, 0, mh]) {
            difference() {
                MutterHalterung(mutterDurchmesser, mutterHoehe, [0,3]);
                cube([ml, rahmen, 5], false);
                cube([rahmen, ml, 5], false);
            }
        }
        translate([laenge - ml, 0, mh]) {
            difference() {
                MutterHalterung(mutterDurchmesser, mutterHoehe, [0,1]);
                cube([ml, rahmen, 5], false);
                translate([ml - rahmen, 0, 0]) cube([rahmen, ml, 5], false);
            }
        }
        translate([laenge - ml, breite - ml - hoeheEinschubGesamt + rahmen, mh]) {
           difference() {
                MutterHalterung(mutterDurchmesser, mutterHoehe, [1]);
                translate([ml - rahmen, 0, 0]) cube([rahmen, ml, 5], false);
            }

        }
        translate([0, breite - ml - hoeheEinschubGesamt + rahmen, mh]) {
        //translate([0, rahmen + 0.15 + (einschubBreite - ml ) / 2, mh]) {
            difference() {
                MutterHalterung(mutterDurchmesser, mutterHoehe, [3]);
                cube([rahmen, ml, 5], false);
            }
        }
        
       // Mutterhalterung Platine rechts: auf 66% der Einschubplatte
        translate([0, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 * 2 - 0.15 * rahmen , zEinschub2 - mutterHoehe * 1.5]) {
            MutterHalterung(mutterDurchmesser, mutterHoehe, [3], true);    
        }
        
        // Mutterhalterung Platine links: auf 33% der Einschubplatte
        translate([laenge - ml, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 - 0.15 * rahmen , zEinschub2 - mutterHoehe * 1.5]) {
            MutterHalterung(mutterDurchmesser, mutterHoehe, [1], true);    
        }
               
        // Mutterhalterung SDS011 rechts: mittig von der Einschubplatte
        translate([rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 2 - 0.15 * rahmen , zEinschub1 - mutterHoehe * 1.5]) {
            MutterHalterung(mutterDurchmesser, mutterHoehe, [3], false);
        }
        
        // Mutterhalterung SDS011 links: mittig von der Einschubplatte
        translate([laenge - ml-rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 2 - 0.15 * rahmen , zEinschub1 - mutterHoehe * 1.5]) {
            MutterHalterung(mutterDurchmesser, mutterHoehe, [1], false);
        }
        
        
        // Mutterhalterung Stativ Boden
        translate([(laenge - mbl) / 2, (breite - mbl) / 2, 0]) 
            rotate_about_pt(90, 180, [mbl / 2, mbl / 2, mbh / 2])
                MutterHalterung(mbd, mbmh);    
        
        if (KINDERSICHERUNG) {
        // Kindersicherung An-/Aus-Schalter
        
        translate([laenge - (laenge/2)/2 - schalterLochradius, 0, hoeheAnAusSchalter])
                rotate_about_pt(-90, 90, [schalterLochradius, 0, rahmen/2]) union() {
            difference() {
    union() {
        s_thread(gewindeDurchmesser,gewindeAbstand,gewindeHoehe);
        cylinder(rahmen, 3*gewindeDurchmesser/4, 3*gewindeDurchmesser/4, false);
        cylinder(rahmen*2, gewindeDurchmesser/2, gewindeDurchmesser/2, false);
    }
    cylinder(gewindeHoehe*2, schalterLochradius*2 +0.4, schalterLochradius*2 +0.4, false);
    //cylinder(gewindeHoehe*2, 2, 2, false);
    
    translate([0, gewindeDurchmesser/2 + (gewindeDurchmesser/8), 0])
        cylinder(0.6, 0.2, 0.2, false);
    
    translate([0, -gewindeDurchmesser/2 - (gewindeDurchmesser/8), 0])
        cylinder(0.6, 0.2, 0.2, false);
}
        }
    }
        
        
        
        // Halterung für Batteriebox
        batterieBoxWaende();
    }
}
            

// allgemeine Einschubebene für Ebenen
module Einschub(a = 0) {
    difference() {
        cube([einschubLaenge, einschubBreite - a, rahmen]);
        // Kabelloch für Einschub
        translate([2.8 + platinenBeginnX + 1.5, einschubBreite - 1 * rahmen, 0]) cylinder(rahmen, 1, 1, false);
    }
}

module Einschub_2D(a = 0) {
    difference() {
        square([einschubLaenge, einschubBreite - a]);
        // Kabelloch für Einschub
        translate([2.8 + platinenBeginnX + 1.5, einschubBreite - 1 * rahmen, 0]) circle(1);
    }
}


module Luftlochreihe(laengeReihe, luftlochradius, anzahlLoecher) {
    freieFlaeche = laengeReihe - 2 *luftlochradius * anzahlLoecher;
    
    abstand = freieFlaeche / (anzahlLoecher + 1);
    for ( i = [1 : anzahlLoecher] ) {
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            rotate_about_pt(90, 90, [luftlochradius, 0, rahmen / 2]) cylinder(rahmen*2, luftlochradius, luftlochradius, false);
        }
    }
}

module Luftlochreihe_2D(laengeReihe, luftlochradius, anzahlLoecher) {
    freieFlaeche = laengeReihe - 2 *luftlochradius * anzahlLoecher;
    
    abstand = freieFlaeche / (anzahlLoecher + 1);
    for ( i = [1 : anzahlLoecher] ) {
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            //rotate_about_pt(90, 90, [luftlochradius, 0, rahmen / 2]) 
            circle(luftlochradius);
        }
    }
}

module LuftlochreiheDeckel(laengeReihe, luftlochradius, anzahlLoecher) {
    freieFlaeche = laengeReihe - 2 *luftlochradius * anzahlLoecher;
    
    abstand = freieFlaeche / (anzahlLoecher + 1);
    for ( i = [1 : anzahlLoecher] ) {
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            cylinder(rahmen*2, luftlochradius, luftlochradius, false);
        }
    }
}

module LuftlochreiheDeckel_2D(laengeReihe, luftlochradius, anzahlLoecher) {
    freieFlaeche = laengeReihe - 2 *luftlochradius * anzahlLoecher;
    
    abstand = freieFlaeche / (anzahlLoecher + 1);
    for ( i = [1 : anzahlLoecher] ) {
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            circle(luftlochradius);
        }
    }
}

module LuftlochFlaecheDeckel(breiteFlaeche, luftlochradius, anzahlReihen) {
    freieFlaeche = breiteFlaeche - 2 *luftlochradius * anzahlReihen;
    
    abstand = freieFlaeche / (anzahlReihen + 1);
    
    for ( i = [1 : anzahlReihen] ) {
        translate([0, abstand + (i-1) * (luftlochradius * 2 + abstand), 0]) {
            LuftlochreiheDeckel(deckelLuftLaenge, luftlochRadiusDeckel, luftlochAnzahlDeckel);
        }
    }
    
    
    
    
    
    /*
    for ( i = [1 : anzahlReihen] ) {
        /*
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            cylinder(rahmen*2, luftlochradius, luftlochradius, false);
            // * /
            translate([0, abstand + luftlochReiheAbstandDeckel * (i-1), 0]) {
            LuftlochreiheDeckel(deckelLuftLaenge, luftlochRadiusDeckel, luftlochAnzahlDeckel);
        }
    }
    */
}

module LuftlochFlaecheDeckel_2D(breiteFlaeche, luftlochradius, anzahlReihen) {
    freieFlaeche = breiteFlaeche - 2 *luftlochradius * anzahlReihen;
    
    abstand = freieFlaeche / (anzahlReihen + 1);
    
    for ( i = [1 : anzahlReihen] ) {
        translate([0, abstand + (i-1) * (luftlochradius * 2 + abstand), 0]) {
            LuftlochreiheDeckel_2D(deckelLuftLaenge, luftlochRadiusDeckel, luftlochAnzahlDeckel);
        }
    }
    
    
    
    
    
    /*
    for ( i = [1 : anzahlReihen] ) {
        /*
        translate([abstand + (i-1) * (luftlochradius * 2 + abstand), rahmen / 2, 0]) {
            cylinder(rahmen*2, luftlochradius, luftlochradius, false);
            // * /
            translate([0, abstand + luftlochReiheAbstandDeckel * (i-1), 0]) {
            LuftlochreiheDeckel(deckelLuftLaenge, luftlochRadiusDeckel, luftlochAnzahlDeckel);
        }
    }
    */
}

// Box
if (BOX) {
    difference() {
        Box();
        // leicht abstehende Mutterhalterung am Boden wegschneiden
        translate([0, 0, -0.9994])cube([laenge, breite, 1], false);
    }
}


/*
translate([rahmen * 1.15, rahmen * 1.15, zEinschub1 + rahmen]) {
// rechts bei Abluftlöchern   
            translate([rahmen * 2.15, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5 + 0.05, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            
            // links bei Ansaugloch  
            translate([laenge - ml, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5  + 0.05, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
        }
*/


// Einschub SDS011 3D
if (EINSCHUB_SDS_3D) {
    translate([rahmen * 1.15, rahmen * 1.15, zEinschub1 + rahmen]) {
        
        color("Green");
        difference() {
            // Einschubplatte
            Einschub(0.2);
            // SDS011 Befestigungslöcher
            translate([einschubLaenge / 2 - 7 / 2, einschubBreite / 2 - 7 / 2-1, 0]) {
                
                rotate_about_pt(90, 0, [3.5, 3.5, 0]) union() {
                    translate([0.5, 1.1, 0]) cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
                    translate([6.5, 1.1, 0]) cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
                    translate([4.6, 6.6, 0]) cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
                    
                }  
            }
            
            
            //Halterungslöcher für Einschubplatte            //translate([(ml/2)-(lochradiusSchraubeEinschub)+0.4 * rahmen, (breite - ml -hoeheEinschubGesamt) / 2 +0.45 * rahmen, 0])
            // rechts bei Abluftlöchern   
            translate([rahmen * 2.15, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5 + 0.05, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            
            // links bei Ansaugloch  
            translate([laenge - ml, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5  + 0.05, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
        }
        
    }
}

// Einschub SDS011 2D
if (EINSCHUB_SDS_2D) {
    translate([rahmen * 1.15, rahmen * 1.15, zEinschub1 + rahmen]) {
        
        color("Green");
        difference() {
            // Einschubplatte
            Einschub_2D(0.2);
            // SDS011 Befestigungslöcher
            translate([einschubLaenge / 2 - 7 / 2, einschubBreite / 2 - 7 / 2-1, 0]) {
                
                rotate_about_pt(90, 0, [3.5, 3.5, 0]) union() {
                    translate([0.5, 1.1, 0]) circle(lochradiusSchraubeEinschub);
                    translate([6.5, 1.1, 0]) circle(lochradiusSchraubeEinschub);
                    translate([4.6, 6.6, 0]) circle(lochradiusSchraubeEinschub);
                    
                }  
            }
            
            
            //Halterungslöcher für Einschubplatte            //translate([(ml/2)-(lochradiusSchraubeEinschub)+0.4 * rahmen, (breite - ml -hoeheEinschubGesamt) / 2 +0.45 * rahmen, 0])
            // rechts bei Abluftlöchern   
            translate([rahmen * 2.15, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5  + 0.05, 0])
                circle(lochradiusSchraubeEinschub);
            
            // links bei Ansaugloch  
            translate([laenge - ml, (einschubBreite - 0.2) / 2 - ml/2 + rahmen * 1.15 *0.5  + 0.05, 0])
                circle(lochradiusSchraubeEinschub);
        }
        
    }
}


// Einschub Platine 3D
if (EINSCHUB_PLATINE_3D) {
    translate([rahmen * 1.15, rahmen * 1.15, zEinschub2 + rahmen]) {
        
        difference() {
            // Einschubplatte
            Einschub(0.2);
            // Platine Befestigungslöcher
            translate([platinenBeginnX, platinenBeginnY-rahmen * 1.15, 0]) {
                //Loch oben rechte
                translate([platinenLaenge - 5, 3.2, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
                //Loch unten rechts
                translate([platinenLaenge - 5, 6.5, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
                //Loch links
                translate([platinenLaenge - 1.4, 2, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            }
            
            
            //Halterungslöcher für Einschubplatte            
            // rechts   
            translate([rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 * 2 + 0.9 * rahmen, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            
            // links  
            translate([laenge - ml+rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 + 0.9 * rahmen, 0])
                cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
        }
        
    }
}

// Einschub Platine 2D
if (EINSCHUB_PLATINE_2D) {
    translate([rahmen * 1.15, rahmen * 1.15, zEinschub2 + rahmen]) {
       
        difference() {
            // Einschubplatte
            Einschub_2D(0.2);
            // Platine Befestigungslöcher
            translate([platinenBeginnX, platinenBeginnY-rahmen * 1.15, 0]) {
                //Loch oben rechte
                translate([platinenLaenge - 5, 3.2, 0])
                circle(lochradiusSchraubeEinschub);
                //Loch unten rechts
                translate([platinenLaenge - 5, 6.5, 0])
                circle(lochradiusSchraubeEinschub);
                //Loch links
                translate([platinenLaenge - 1.4, 2, 0])
                circle(lochradiusSchraubeEinschub);
            }
            
            
            //Halterungslöcher für Einschubplatte            
            // rechts   
            translate([rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 * 2 + 0.9 * rahmen, 0])
                circle(lochradiusSchraubeEinschub);
            
            // links  
            translate([laenge - ml+rahmen, (breite - 0.2 - ml - hoeheEinschubGesamt) / 3 + 0.9 * rahmen, 0])
                circle(lochradiusSchraubeEinschub);
        }
        
    }
}


// Platine
if (PLATINE) {
    translate([rahmen * 1.15, 0, 0]) {
    translate([platinenBeginnX, platinenBeginnY, zEinschub2 + 2*rahmen]) {
        difference() {
            cube([platinenLaenge, platinenBreite, platineMitBauteilenHoehe]);
            //cube([platinenLaenge, platinenBreite, platinenDicke]);
            //Loch oben rechte
            translate([platinenLaenge - 5, 3.2, 0])
            cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            //Loch unten rechts
            translate([platinenLaenge - 5, 6.5, 0])
            cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
            //Loch links
            translate([platinenLaenge - 1.4, 2, 0])
            cylinder(rahmen, lochradiusSchraubeEinschub, lochradiusSchraubeEinschub, false);
        }
    }
}
}


// Deckel 3D
if (DECKEL_3D) {
    //translate([deckelRandVersatz, deckelRandVersatz, hoehe]) {
        
        difference() {
            // Deckelplatte
            translate([deckelRandVersatz, deckelRandVersatz, hoehe]) roundedcube([deckelLaenge, deckelBreite, deckelDicke], false, rundung);
            //cube([deckelLaenge, deckelBreite, deckelDicke]);
            
            
            // Luftlöcher von hinten angefangen
            translate([deckelLuftBeginnX + rahmen, deckelLuftBeginnY, hoehe]) {
                // Erzeuge eine Fläche von Luftlochreihen
                LuftlochFlaecheDeckel(deckelLuftBreite, luftlochRadiusDeckel, luftlochAnzahlDeckel);
            }
            
            
            //Halterungslöcher für Deckel  
            translate([rahmen * 1.15, rahmen * 1.15, hoehe]) {
                // hinten rechts (am KUS) 
                translate([rahmen, rahmen, 0])
                    cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
                
                // vorne rechts 
                translate([rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                    cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
                
                // hinten links  
                translate([laenge - ml + rahmen, rahmen, 0])
                    cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
                
                // vorne links 
                translate([laenge - ml + rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                    cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
                
                }
            }
}

/*
// Tests für Deckel Halterungen
translate([rahmen * 1.15, rahmen * 1.15, hoehe]) {
translate([rahmen, rahmen, 0])
                cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);

translate([rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
    
translate([laenge - ml + rahmen, rahmen, 0])
                cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);
    
    translate([laenge - ml + rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                cylinder(rahmen, lochradiusSchraubeDeckel, lochradiusSchraubeDeckel, false);

}
*/


// Deckel 2D
if (DECKEL_2D) {
    //translate([deckelRandVersatz, deckelRandVersatz, hoehe]) {
        
        difference() {
            // Deckelplatte
            translate([deckelRandVersatz, deckelRandVersatz, hoehe]) square([deckelLaenge, deckelBreite]);           
            
            // Luftlöcher von hinten angefangen
            translate([deckelLuftBeginnX + rahmen, deckelLuftBeginnY, hoehe]) {
                // Erzeuge eine Fläche von Luftlochreihen
                LuftlochFlaecheDeckel_2D(deckelLuftBreite, luftlochRadiusDeckel, luftlochAnzahlDeckel);
            }
            
            
            //Halterungslöcher für Deckel  
            translate([rahmen * 1.15, rahmen * 1.15, hoehe]) {
                // hinten rechts (am KUS) 
                translate([rahmen, rahmen, 0])
                    circle(lochradiusSchraubeDeckel);
                
                // vorne rechts 
                translate([rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                    circle(lochradiusSchraubeDeckel);
                
                // hinten links  
                translate([laenge - ml + rahmen, rahmen, 0])
                    circle(lochradiusSchraubeDeckel);
                
                // vorne links 
                translate([laenge - ml + rahmen, breite - ml - hoeheEinschubGesamt + rahmen*2.15, 0])
                    circle(lochradiusSchraubeDeckel);
                
                }
            }
}

// TUER 3D
if (TUER_3D) {
    translate([rahmen, breite - rahmen - breiteEinschub, boden - 3*rahmen ]) {
        difference() {
            // Tuerplatte
            roundedcube([tuerBreite, tuerLaenge, tuerHoehe], false, rundung);
            // Luftlöcher bei Raum 3
            // Luftloecher an Tuer
            translate([0.6, 0, hoeheLuftlochreihen - boden + 3 * rahmen])
                Luftlochreihe(laenge - 2 * 0.6, 0.2, 10);
        }
    }                
}
        
// TUER 2D
if (TUER_2D) {
    translate([rahmen, breite - rahmen - breiteEinschub, boden - 3*rahmen ]) {
        difference() {
            // Tuerplatte
            square([tuerBreite, tuerHoehe]);
            // Luftlöcher bei Raum 3
            // Luftloecher an Tuer
            translate([0.6, hoeheLuftlochreihen - boden + 3 * rahmen], 0)
                Luftlochreihe_2D(laenge - 2 * 0.6, 0.2, 10);
        }
    }                
}

// BATTERIE_BOX
if (BATTERIE_BOX) {
    translate([batterieBoxBeginnX, batterieBoxBeginnY, boden]) { 
        cube([batterieBoxLaenge, batterieBoxBreite, 4], false);           
    }
}

// SDS011
if (SDS011) {
    translate([batterieBoxBeginnX, batterieBoxBeginnY, zBodenRaum2]) { 
        cube([7, 7, hoeheSDS011], false);           
    }
}