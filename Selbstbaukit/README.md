# Selbstbaukit LÜFFI

In diesem Ordner befinden sich alle Dateien, die zum Bauen von Sensorknoten des LÜFFI-Systems benötigt werden.

## Platine (Gerber-Datei)

## Gehäuse (3D-Druck)

## Kindersicherung (3D-Druck)

## Einschubplatte Platine (Lasercutter)

## Einschubplatte Feinstaubsensor SDS011 (Lasercutter)

## Deckel (Lasercutter)

## Wandöffnung (Lasercutter)
