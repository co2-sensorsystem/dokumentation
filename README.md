# CO2-Sensornetzwerk - Dokumentation

In diesem Repository befindet sich die Dokumentation des CO2-Sensornetzwerkes.

- [Handbuch](handbuch/Handbuch.pdf)
- [Poster](poster/Poster.pdf)
- [Ausarbeitung](ausarbeitung/Ausarbeitung.pdf)

Eine Installationsanleitung für die Serverkomponenten befindet sich im [Handbuch](handbuch/Handbuch.pdf).  
Eine Bauanleitung für die Senorknoten findet sich in der [Ausarbeitung](ausarbeitung/Ausarbietung.pdf).


# Pressemitteilungen

**Lüftungskonzepte einfach überprüfen: Studierende entwickeln CO2-Messung mit mehreren Sensoren**, 30.07.2021, Carl von Ossietzky Universität Oldenburg:

https://www.presse.uni-oldenburg.de/mit/2021/142.html


**Test für Lüftungskonzepte entwickelt**, 12.08.2021, Nordwest-Zeitung, Nr. 186.
Online als **Studenten entwickeln Test für Lüftungskonzepte** erhältlich unter:

https://www.nwzonline.de/plus-oldenburg-stadt/oldenburg-forschung-studenten-entwickeln-test-fuer-lueftungskonzepte_a_51,2,4085160121.html


**Lüftungskonzepte einfach überprüfen**, 31.08.2021, Carl von Ossietzky Universität Oldenburg:

https://uol.de/aktuelles/artikel/lueftungskonzepte-einfach-ueberpruefen-5348
